# HealBot Ascended
This project is in early stages. There may be issues. Please post them accordingly with detail.

A work-in-progress update to HealBot to accomodate Project Ascension.
**THIS PROJECT IS NOT AFFILIATED WITH** 'Project Ascension' **IN ANY WAY.**

## Changelog

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/), and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Todo]
 - Cleanup config builder for hots/buffs.
 

## [Unreleased]


## [v0.0.1]
### Added
  - 'Seed of Life' as an ability under Druid.
  - 'Frugal Disposition' as an ability under Druid. 

### Removed
  - Checks for class when using abilities that prevented icons from showing.